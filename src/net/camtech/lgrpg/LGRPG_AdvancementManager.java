package net.camtech.lgrpg;

import net.camtech.camutils.CUtils_Advancement;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.bukkit.Bukkit;

public class LGRPG_AdvancementManager
{
    public static CUtils_Advancement createAdvancement(String key, String title, String description, String icon)
    {
        CUtils_Advancement advancement = new CUtils_Advancement(new NamespacedKey(LynxGamingRPG.plugin, key))
                .withTitle(title)
                .withDescription(description)
                .withIcon(icon)
                .withTrigger("minecraft:impossible")
                .withBackground("minecraft:textures/gui/advancements/backgrounds/stone.png")
                .withFrame(CUtils_Advancement.FrameType.GOAL)
                .withItem(new ItemStack(Material.NETHER_STAR, 1))
                .withParent("lynxgamingrpg:mainadvancement");
        System.out.println(new NamespacedKey(LynxGamingRPG.plugin, key).toString());
        return advancement;
    }
}
