package net.camtech.lgrpg;

import java.util.ArrayList;
import static net.camtech.lgrpg.LynxGamingRPG.players;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;
import org.bukkit.scoreboard.Team;

public class LGRPG_BoardManager {
    
    public static ScoreboardManager manager = Bukkit.getScoreboardManager();
    
    public static void updateStats(Player player)
    {
        Scoreboard board = manager.getNewScoreboard();
        String name = player.getName();
        Objective o = board.getObjective("stats");
        if(o == null)
        {
            o = board.registerNewObjective("stats", "dummy");
            o.setDisplaySlot(DisplaySlot.SIDEBAR);
            o.setDisplayName(ChatColor.GOLD + "" + ChatColor.MAGIC + "|@|" + ChatColor.BLUE + "Your Stats" + ChatColor.GOLD + "" + ChatColor.MAGIC + "|@|");
        }
        FileConfiguration config = players.getConfig();
        String vocation = config.getString("players." + name + ".class");
        Score voc = o.getScore(ChatColor.GOLD + "Class: ");
        Score voc2 = o.getScore(ChatColor.RED + " " + vocation);
        Score level = o.getScore(ChatColor.GOLD + "Level: " + ChatColor.RED + config.getInt("players." + name + ".classes." + vocation.toLowerCase() + ".level"));

        String xpString;
        long xpVal = (long) config.getDouble("players." + name + ".classes." + vocation.toLowerCase() + ".xp");
        if(xpVal > 100000000)
        {
            xpString = (xpVal / 1000000) + "m";
        }
        else if(xpVal > 100000)
        {
            xpString = (xpVal / 1000) + "k";
        }
        else
        {
            xpString = "" + xpVal;
        }
        String nextString;
        long toNext = (long) config.getDouble("players." + name + ".classes." + vocation.toLowerCase() + ".toNext");
        if(toNext > 100000000)
        {
            nextString = (toNext / 1000000) + "m";
        }
        else if(toNext > 100000)
        {
            nextString = (toNext / 1000) + "k";
        }
        else
        {
            nextString = "" + toNext;
        }
        Score xp = o.getScore(ChatColor.GOLD + "XP: " + ChatColor.RED + xpString);
        Score next = o.getScore(ChatColor.GOLD + "Next: " + ChatColor.RED + nextString);
        Score lives = o.getScore(ChatColor.GOLD + "Lives: " + ChatColor.RED + config.getInt("players." + name + ".classes." + vocation.toLowerCase() + ".lives"));
        
        next.setScore(1);
        xp.setScore(2);
        level.setScore(3);
        voc2.setScore(4);
        voc.setScore(5);
        lives.setScore(6);
        
        Team warrior = board.registerNewTeam("Warrior");
        Team mage = board.registerNewTeam("Mage");
        Team archer = board.registerNewTeam("Archer");
        Team overlord = board.registerNewTeam("Overlord");
        
        warrior.setPrefix(ChatColor.RED + "");
        mage.setPrefix(ChatColor.BLUE + "");
        archer.setPrefix(ChatColor.GREEN + "");
        overlord.setPrefix(ChatColor.DARK_PURPLE + "");
        
        ArrayList<Team> teams = new ArrayList<>();
        teams.add(warrior);
        teams.add(mage);
        teams.add(archer);
        teams.add(overlord);
        
        for(Player p : Bukkit.getOnlinePlayers())
        {
            String voc1 = config.getString("players." + p.getName() + ".class");
            for(Team t : teams)
            {
                if(voc1.equals(t.getName()))
                {
                    t.addPlayer(p);
                }
            }
        }
        
        player.setScoreboard(board);
        players.saveConfig();
    }
}
