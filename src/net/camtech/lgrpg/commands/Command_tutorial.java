package net.camtech.lgrpg.commands;

import net.camtech.camutils.CUtils_Methods;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class Command_tutorial implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
            cs.sendMessage(CUtils_Methods.colour("&9&lWelcome to LGRPG! &6A unique plugin based around adding RPG elements to Minecraft! Try &5/class&6 to start! Once in /class, select a class (only Mage works at the moment and Warrior has two abilities) and give yourself the appropriate item as shown on the icon for each class. (I.E sword for Warrior, blaze rod for Mage etc.) and do the clicks listed on their descriptions (i.e, a Mage can perform 'Fireball' by doing Right Click, Right Click, Right Click) to perform spells and gain experience, the more experience you have, the higher your level and the more spells you unlock! ENJOY!!"));
            cs.sendMessage(ChatColor.DARK_AQUA + "Side Note: If you press shift whilst on the ground, you will create a temporary glowstone block on the ground beneath you, handy for lighting up a cave you're exploring! You can also hold shift when you are going to take fall damage and negate the damage for a bit of mana instead!");
            return true;
    }
    
}
