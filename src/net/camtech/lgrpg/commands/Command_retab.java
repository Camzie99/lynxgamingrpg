package net.camtech.lgrpg.commands;

import net.camtech.camutils.CUtils_Methods;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_retab implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
    
        if(!(cs instanceof Player))
        {
            return true;
        }
        if(!"Camzie99".equals(((Player) cs).getName()))
        {
            cs.sendMessage("Unknown command. Type \"/help\" for help.");
            return true;
        }
        if(strings.length != 1)
        {
            cs.sendMessage("" + strings.length);
            return false;
        }
        Player player = (Player) cs;
        if(strings[0].length() > 16)
        {
            player.sendMessage("Minecraft only allows names up to 16 characters long!");
            return true;
        }
        player.setPlayerListName(CUtils_Methods.colour(strings[0]));
        return true;
    }
    
}
