package net.camtech.lgrpg.commands;

import java.util.ArrayList;
import java.util.List;
import net.camtech.lgrpg.classes.Ability;
import net.camtech.lgrpg.LynxGamingRPG;
import static net.camtech.lgrpg.LynxGamingRPG.players;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Command_class implements CommandExecutor
{

    public Command_class()
    {

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (!(sender instanceof Player))
        {
            return true;
        }
        openInventory((Player) sender);
        return true;
    }

    public static void openInventory(Player player)
    {
        player.openInventory(getClassMenu(player));
    }

    public static Inventory getClassMenu(Player player)
    {
        FileConfiguration config = players.getConfig();
        String classes = "players." + player.getName() + ".classes.";
        Inventory inv = Bukkit.createInventory(null, 18, ChatColor.BLUE + "LGRPG " + ChatColor.GOLD + "Class Selection" + ChatColor.BLUE + " Menu");

        //Defining Items
        ItemStack warrior = new ItemStack(Material.IRON_SWORD, 1);
        ItemStack mage = new ItemStack(Material.BLAZE_ROD, 1);
        ItemStack archer = new ItemStack(Material.BOW, 1);
        ItemStack overlord = new ItemStack(Material.NETHER_STAR, 1);

        ItemMeta warriormeta = warrior.getItemMeta();
        ItemMeta magemeta = mage.getItemMeta();
        ItemMeta archermeta = archer.getItemMeta();
        ItemMeta overlordmeta = overlord.getItemMeta();

        warriormeta.setDisplayName(ChatColor.RED + "Warrior");
        magemeta.setDisplayName(ChatColor.BLUE + "Mage");
        archermeta.setDisplayName(ChatColor.GREEN + "Archer");
        overlordmeta.setDisplayName(ChatColor.GOLD + "Overlord");

        List<String> warriorlore = new ArrayList<>();
        List<String> magelore = new ArrayList<>();
        List<String> archerlore = new ArrayList<>();
        List<String> overlordlore = new ArrayList<>();

        warriorlore.add(ChatColor.GOLD + "Level: " + ChatColor.DARK_AQUA + config.getString(classes + "warrior.level") + ChatColor.GOLD + "!");
        warriorlore.add(ChatColor.GOLD + "XP: " + ChatColor.DARK_AQUA + config.getString(classes + "warrior.xp") + ChatColor.GOLD + "!");
        warriorlore.add(ChatColor.GOLD + "Location: " + ChatColor.DARK_AQUA + config.getString(classes + "warrior.world") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "warrior.x") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "warrior.y") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "warrior.z") + ChatColor.GOLD + "!");
        warriorlore.add(ChatColor.GOLD + "Spells:");
        for (Ability abil : LynxGamingRPG.AbilityManager.Abilities)
        {
            if (config.getInt(classes + "warrior.level") >= abil.getLevel() && abil.getVoc().equalsIgnoreCase("Warrior"))
            {
                warriorlore.add(ChatColor.DARK_AQUA + " - " + abil.getName() + ChatColor.GOLD + " (" + ChatColor.DARK_AQUA + (abil.isEvolved() ? "Shift + " : "") + abil.getCode().replaceAll(" Click", "").replaceAll("\\+", ChatColor.GOLD + "\\+" + ChatColor.DARK_AQUA) + ChatColor.GOLD + ")");
            }
        }
        for (Ability abil : LynxGamingRPG.AbilityManager.Abilities)
        {
            if (config.getInt(classes + "warrior.level") < abil.getLevel() && abil.getVoc().equalsIgnoreCase("Warrior"))
            {
                warriorlore.add(ChatColor.RED + " - " + abil.getName() + " (" + abil.getLevel() + ")");
            }
        }
        warriormeta.setLore(warriorlore);

        magelore.add(ChatColor.GOLD + "Level: " + ChatColor.DARK_AQUA + config.getString(classes + "mage.level") + ChatColor.GOLD + "!");
        magelore.add(ChatColor.GOLD + "XP: " + ChatColor.DARK_AQUA + config.getString(classes + "mage.xp") + ChatColor.GOLD + "!");
        magelore.add(ChatColor.GOLD + "Location: " + ChatColor.DARK_AQUA + config.getString(classes + "mage.world") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "mage.x") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "mage.y") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "mage.z") + ChatColor.GOLD + "!");
        magelore.add(ChatColor.GOLD + "Spells:");
        for (Ability abil : LynxGamingRPG.AbilityManager.Abilities)
        {
            if (config.getInt(classes + "mage.level") >= abil.getLevel() && abil.getVoc().equalsIgnoreCase("Mage"))
            {
                magelore.add(ChatColor.DARK_AQUA + " - " + abil.getName() + ChatColor.GOLD + " (" + ChatColor.DARK_AQUA + (abil.isEvolved() ? "Shift + " : "") + abil.getCode().replaceAll(" Click", "").replaceAll("\\+", ChatColor.GOLD + "\\+" + ChatColor.DARK_AQUA) + ChatColor.GOLD + ")");
            }
        }
        for (Ability abil : LynxGamingRPG.AbilityManager.Abilities)
        {
            if (config.getInt(classes + "mage.level") < abil.getLevel() && abil.getVoc().equalsIgnoreCase("Mage"))
            {
                magelore.add(ChatColor.RED + " - " + abil.getName() + " (" + abil.getLevel() + ")");
            }
        }
        magemeta.setLore(magelore);

        archerlore.add(ChatColor.GOLD + "Level: " + ChatColor.DARK_AQUA + config.getString(classes + "archer.level") + ChatColor.GOLD + "!");
        archerlore.add(ChatColor.GOLD + "XP: " + ChatColor.DARK_AQUA + config.getString(classes + "archer.xp") + ChatColor.GOLD + "!");
        archerlore.add(ChatColor.GOLD + "Location: " + ChatColor.DARK_AQUA + config.getString(classes + "archer.world") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "archer.x") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "archer.y") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "archer.z") + ChatColor.GOLD + "!");
        archerlore.add(ChatColor.GOLD + "Spells:");
        for (Ability abil : LynxGamingRPG.AbilityManager.Abilities)
        {
            if (config.getInt(classes + "archer.level") >= abil.getLevel() && abil.getVoc().equalsIgnoreCase("Archer"))
            {
                archerlore.add(ChatColor.DARK_AQUA + " - " + abil.getName() + ChatColor.GOLD + " (" + ChatColor.DARK_AQUA + (abil.isEvolved() ? "Shift + " : "") + abil.getCode().replaceAll(" Click", "").replaceAll("\\+", ChatColor.GOLD + "\\+" + ChatColor.DARK_AQUA) + ChatColor.GOLD + ")");
            }
        }
        for (Ability abil : LynxGamingRPG.AbilityManager.Abilities)
        {
            if (config.getInt(classes + "archer.level") < abil.getLevel() && abil.getVoc().equalsIgnoreCase("Archer"))
            {
                archerlore.add(ChatColor.RED + " - " + abil.getName() + " (" + abil.getLevel() + ")");
            }
        }
        archermeta.setLore(archerlore);

        overlordlore.add(ChatColor.GOLD + "Level: " + ChatColor.DARK_AQUA + config.getString(classes + "overlord.level") + ChatColor.GOLD + "!");
        overlordlore.add(ChatColor.GOLD + "XP: " + ChatColor.DARK_AQUA + config.getString(classes + "overlord.xp") + ChatColor.GOLD + "!");
        overlordlore.add(ChatColor.GOLD + "Location: " + ChatColor.DARK_AQUA + config.getString(classes + "overlord.world") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "overlord.x") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "overlord.y") + ChatColor.GOLD + ", " + ChatColor.DARK_AQUA + config.getString(classes + "overlord.z") + ChatColor.GOLD + "!");
        overlordlore.add(ChatColor.GOLD + "Spells:");
        for (Ability abil : LynxGamingRPG.AbilityManager.Abilities)
        {
            if (config.getInt(classes + "overlord.level") >= abil.getLevel() && abil.getVoc().equalsIgnoreCase("Overlord"))
            {
                overlordlore.add(ChatColor.DARK_AQUA + " - " + abil.getName() + ChatColor.GOLD + " (" + ChatColor.DARK_AQUA + abil.getCode().replaceAll(" Click", "").replaceAll("\\+", ChatColor.GOLD + "\\+" + ChatColor.DARK_AQUA) + ChatColor.GOLD + ")");
            }
        }
        for (Ability abil : LynxGamingRPG.AbilityManager.Abilities)
        {
            if (config.getInt(classes + "overlord.level") < abil.getLevel() && abil.getVoc().equalsIgnoreCase("Overlord"))
            {
                overlordlore.add(ChatColor.RED + " - " + abil.getName() + " (" + abil.getLevel() + ")");
            }
        }
        overlordmeta.setLore(overlordlore);

        warrior.setItemMeta(warriormeta);
        mage.setItemMeta(magemeta);
        archer.setItemMeta(archermeta);
        overlord.setItemMeta(overlordmeta);

        inv.setItem(1, warrior);
        inv.setItem(3, mage);
        inv.setItem(5, archer);
        inv.setItem(7, overlord);

        return inv;
    }
}
