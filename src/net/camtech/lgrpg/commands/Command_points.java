package net.camtech.lgrpg.commands;

import static net.camtech.lgrpg.LynxGamingRPG.players;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_points implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        if(!(cs instanceof Player))
        {
            return true;
        }
        if(strings.length != 1)
        {
            return false;
        }
        players.getConfig().set("players." + cs.getName() + ".points", Integer.parseInt(strings[0]));
        return true;
    }
    
}
