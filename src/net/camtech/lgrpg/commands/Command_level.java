package net.camtech.lgrpg.commands;

import net.camtech.lgrpg.LGRPG_BoardManager;
import net.camtech.lgrpg.LynxGamingRPG;
import static net.camtech.lgrpg.LynxGamingRPG.players;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_level implements CommandExecutor{

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] strings) {
        if(!(cs instanceof Player))
        {
            return true;
        }
        if(strings.length != 1)
        {
            return false;
        }
        if(strings[0].equalsIgnoreCase("reset"))
        {
            String vocation = players.getConfig().getString("players." + cs.getName() + ".class");
            String path = "players." + cs.getName() + ".classes." + vocation.toLowerCase() + ".";
            players.getConfig().set(path + "xp", 0);
            players.getConfig().set(path + "level", 1);
            players.getConfig().set(path + "toNext", 100);
            players.saveConfig();
            LGRPG_BoardManager.updateStats((Player) cs);
            return true;
        }
        else
        {
            double xp = Double.parseDouble(strings[0]);
            LynxGamingRPG.AbilityManager.addXp((Player) cs, xp);
        }
        return true;
    }
    
}
