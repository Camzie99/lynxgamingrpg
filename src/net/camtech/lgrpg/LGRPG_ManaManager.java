package net.camtech.lgrpg;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class LGRPG_ManaManager
{

    private static HashMap<Player, Double> manaAmounts = new HashMap<>();
    private static HashMap<Player, Integer> cooldown = new HashMap<>();

    public static void refillMana()
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                for (Player player : Bukkit.getOnlinePlayers())
                {
                    if (cooldown.containsKey(player))
                    {
                        if (cooldown.get(player) > 0)
                        {
                            cooldown.put(player, cooldown.get(player) - 10);
                        }
                        else
                        {
                            cooldown.remove(player);
                            addMana(player, getMaximumMana(player) / 7.5);
                        }
                    }
                    else
                    {
                        addMana(player, getMaximumMana(player) / 7.5);
                    }
                }
                refillMana();
            }
        }.runTaskLaterAsynchronously(LynxGamingRPG.plugin, 10L);
    }

    public static void addMana(Player player, double amount)
    {
        boolean isEqual = getMana(player) >= getMaximumMana(player);
        if (!manaAmounts.containsKey(player))
        {
            manaAmounts.put(player, getMaximumMana(player));
        }
        else
        {
            manaAmounts.put(player, Math.min(getMaximumMana(player), getMana(player) + amount));
        }
        if(!isEqual)
        {
            sendManaMessage(player);
        }
    }

    public static boolean takeMana(Player player, double amount)
    {
        if (!manaAmounts.containsKey(player))
        {
            manaAmounts.put(player, getMaximumMana(player));
        }
        if (getMana(player) >= amount)
        {
            manaAmounts.put(player, getMana(player) - amount);
            sendManaMessage(player);
            cooldown.put(player, 40);
            return true;
        }
        sendInsufficientManaMessage(player);
        return false;
    }

    public static double getMana(Player player)
    {
        if (!manaAmounts.containsKey(player))
        {
            manaAmounts.put(player, getMaximumMana(player));
        }
        return manaAmounts.get(player);
    }

    public static double getMaximumMana(Player player)
    {
        return 10d + LynxGamingRPG.getSkill(player, "intelligence") / 10d;
    }

    private static void sendManaMessage(Player player)
    {
        double percent = (getMana(player) / getMaximumMana(player)) * 100d;
        int amount = (int) Math.ceil(percent);
        StringBuilder builder = new StringBuilder();
        builder.append(ChatColor.DARK_AQUA);
        for (int i = 1; i <= 100; i++)
        {
            if (i == amount + 1)
            {
                builder.append(ChatColor.AQUA);
            }
            builder.append("|");
        }
        BountifulAPI.sendActionBar(player, builder.toString());
    }

    private static void sendInsufficientManaMessage(Player player)
    {
        double percent = (getMana(player) / getMaximumMana(player)) * 100d;
        int amount = (int) Math.ceil(percent);
        StringBuilder builder = new StringBuilder();
        builder.append(ChatColor.RED);
        for (int i = 1; i <= 100; i++)
        {
            if (i == amount + 1)
            {
                builder.append(ChatColor.DARK_RED);
            }
            builder.append("|");
        }
        BountifulAPI.sendActionBar(player, builder.toString());
        BountifulAPI.sendTitle(player, 5, 5, 40, ChatColor.RED + "Insufficient Mana", ChatColor.DARK_RED + "" + (Math.floor(getMana(player) * 10d) / 10d) + "/" + ChatColor.RED + getMaximumMana(player));
    }

    public static void setMana(Player player, double amount)
    {
        manaAmounts.put(player, amount);
        sendInsufficientManaMessage(player);
    }
}
