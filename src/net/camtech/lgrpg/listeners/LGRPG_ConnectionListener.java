package net.camtech.lgrpg.listeners;

import java.util.ArrayList;
import net.camtech.camutils.CUtils_Methods;
import net.camtech.camutils.CUtils_Player;
import static net.camtech.lgrpg.LGRPG_BoardManager.updateStats;
import net.camtech.lgrpg.LynxGamingRPG;
import static net.camtech.lgrpg.LynxGamingRPG.plugin;
import static net.camtech.lgrpg.listeners.LGRPG_ClassListener.setClass;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class LGRPG_ConnectionListener implements Listener{
    
    public LGRPG_ConnectionListener()
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        final Player p = event.getPlayer();
        CUtils_Player player = new CUtils_Player(p);
        String name = player.getName();
        FileConfiguration config = LynxGamingRPG.players.getConfig();
        if(config.get("players." + name + ".login") != null)
        {
            String orig = config.getString("players." + name + ".class");
            String vocation = orig.toLowerCase();
            setClass(p, orig);
            updateStats(p);
        }
        else
        {
            double x = p.getWorld().getSpawnLocation().getX();
            double y = p.getWorld().getSpawnLocation().getY();
            double z = p.getWorld().getSpawnLocation().getZ();
            
            config.set("players." + name + ".class", "Warrior");
            config.set("players." + name + ".login", "default");
            config.set("players." + name + ".ip", p.getAddress().getAddress());
            config.set("players." + name + ".points", 0);
            for(String skill : LynxGamingRPG.skills)
            {
                config.set("players." + name + ".skills." + skill, 0);
            }
            ArrayList<String> classes = new ArrayList<>();
            classes.add("mage");
            classes.add("warrior");
            classes.add("archer");
            classes.add("overlord");
            for(String current : classes)
            {
                config.set("players." + name + ".classes." + current + ".level", 1);
                config.set("players." + name + ".classes." + current + ".xp", 0);
                config.set("players." + name + ".classes." + current + ".toNext", 100);
                config.set("players." + name + ".classes." + current + ".x", x);
                config.set("players." + name + ".classes." + current + ".y", y);
                config.set("players." + name + ".classes." + current + ".z", z);
                config.set("players." + name + ".classes." + current + ".world", event.getPlayer().getWorld().getName());
            }
            LynxGamingRPG.players.saveConfig();
            Location loc = event.getPlayer().getWorld().getSpawnLocation();
            if(event.getPlayer() == null)
            {
                return;
            }
            setClass(event.getPlayer(), "Warrior");
            String vocation = config.getString("players." + name + ".class");
            updateStats(p);
            p.chat("/tutorial");
            //event.setJoinMessage(ChatColor.BLUE + name +  ChatColor.AQUA + " is a level " + ChatColor.GOLD + 1 + " " + ChatColor.RED + config.getString("players." + name + ".class"));
        }  
        if(!p.getInventory().contains(LynxGamingRPG.skillsBook))
        {
            p.getInventory().setItem(8, LynxGamingRPG.skillsBook);
        }
        config.set("players." + name + ".ip", p.getAddress().getAddress().toString());
    }
    
    private String getCustomLogin(String name)
    {
        FileConfiguration config = LynxGamingRPG.players.getConfig();
        String vocation = config.getString("players." + name + ".class").toLowerCase();
        String login = CUtils_Methods.colour(config.getString("players." + name + ".login"));
        login = login.replaceAll("%level%", config.getString("players." + name + ".classes." + vocation + ".level"));
        login = login.replaceAll("%class%", config.getString("players." + name + ".class"));
        login = login.replaceAll("%xp%", config.getString("player." + name +  ".classes." + vocation + ".xp"));
        return login;
    }
}
