package net.camtech.lgrpg.listeners;

import java.text.DecimalFormat;
import java.util.ArrayList;
import static net.camtech.lgrpg.LGRPG_BoardManager.updateStats;
import static net.camtech.lgrpg.LynxGamingRPG.getSkill;
import static net.camtech.lgrpg.LynxGamingRPG.mainconfig;
import static net.camtech.lgrpg.LynxGamingRPG.players;
import static net.camtech.lgrpg.LynxGamingRPG.plugin;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class LGRPG_ClassListener implements Listener{

    public LGRPG_ClassListener()
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
    
    @EventHandler
    public void onPlayerInventoryClick(InventoryClickEvent event)
    {
        Player player = (Player) event.getWhoClicked();
        Inventory inventory = event.getInventory();
        if(inventory.getName().equals(ChatColor.BLUE + "LGRPG " + ChatColor.GOLD + "Class Selection" + ChatColor.BLUE + " Menu"))
        {
            FileConfiguration config = players.getConfig();
            event.setCancelled(true);
            switch(event.getCurrentItem().getType())
            {
                case BLAZE_ROD:
                    setClass(player, "Mage");
                    break;
                case IRON_SWORD:
                    setClass(player, "Warrior");
                    break;
                case BOW:
                    setClass(player, "Archer");
                    break;
                case NETHER_STAR:
                    int arl = config.getInt("players." + player.getName() + ".classes.archer.level");
                    int mal = config.getInt("players." + player.getName() + ".classes.mage.level");
                    int wal = config.getInt("players." + player.getName() + ".classes.warrior.level");
                    int totalLevel = arl + mal + wal;
                    if(totalLevel < mainconfig.getConfig().getInt("Classes.overlordLevel"))
                    {
                        player.sendMessage(ChatColor.RED + "You have not reached a total of level " + mainconfig.getConfig().getInt("Classes.overlordLevel") + " yet! Remaining: " + ChatColor.GOLD + (mainconfig.getConfig().getInt("Classes.overlordLevel") - totalLevel) + ChatColor.RED + "!");
                        return;
                    }
                    setClass(player, "Overlord");
                    break;
                default:
                    break;
            }
            player.closeInventory();
        }
        else if(inventory.getName().contains(ChatColor.BLUE + "Skills"))
        {
            event.setCancelled(true);
            ItemMeta meta = event.getCurrentItem().getItemMeta();
            int points = players.getConfig().getInt("players." + player.getName() + ".points");
            String skill = "players." + player.getName() + ".skills." + ChatColor.stripColor(meta.getDisplayName()).toLowerCase();
            if(points >= 1 && event.getCurrentItem().getType() != Material.ENCHANTED_BOOK)
            {
                players.getConfig().set("players." + player.getName() + ".points", points - 1);
                players.getConfig().set(skill, players.getConfig().getInt(skill) + 1);
                String name = ChatColor.stripColor(meta.getDisplayName()).toLowerCase();
                if(name.equals("strength"))
                {
                    strength(player, inventory);
                }
                if(name.equals("dexterity"))
                {
                    dexterity(player, inventory);
                }
                if(name.equals("intelligence"))
                {
                    intelligence(player, inventory);
                }
                if(name.equals("development"))
                {
                    development(player, inventory);
                }
                ItemStack item = new ItemStack(Material.ENCHANTED_BOOK, 1);
                ItemMeta itemmeta = item.getItemMeta();
                ArrayList<String> lore = new ArrayList<>();
                lore.add(ChatColor.GOLD + "Current Skill Points: " + (points - 1));
                itemmeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Skill Points");
                itemmeta.setLore(lore);
                item.setItemMeta(itemmeta);
                inventory.setItem(4, item);
                player.playSound(player.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
            }
        }
    }
    
    public static void setClass(Player player, String vocation)
    {
        String lower = vocation.toLowerCase();
        String name = player.getName();
        FileConfiguration config = players.getConfig();
        String orig = config.getString("players." + name + ".class").toLowerCase();
        String origworld = player.getWorld().getName();
        if(orig.equals(vocation))
        {
            return;
        }
        DecimalFormat df = new DecimalFormat("#.##");
        Double x = player.getLocation().getX();
        Double y = player.getLocation().getY();
        Double z = player.getLocation().getZ();
        
        x = Double.valueOf(df.format(x));
        y = Double.valueOf(df.format(y));
        z = Double.valueOf(df.format(z));
        
        config.set("players." + name + ".classes." + orig + ".x",x);
        config.set("players." + name + ".classes." + orig + ".y",y);
        config.set("players." + name + ".classes." + orig + ".z",z);
        config.set("players." + name + ".classes." + orig + ".world",origworld);
        config.set("players." + name + ".class", vocation);
        
        x = config.getDouble("players." + name + ".classes." + lower + ".x");
        y = config.getDouble("players." + name + ".classes." + lower + ".y");
        z = config.getDouble("players." + name + ".classes." + lower + ".z");
        
        Location loc = new Location(Bukkit.getWorld(config.getString("players." + name + ".classes." + lower + ".world")), x, y, z);
        
        player.teleport(loc);
        players.saveConfig();
        for(Player p : Bukkit.getOnlinePlayers())
        {
            updateStats(player);
        }
        
        if(Bukkit.getPluginManager().isPluginEnabled("FreedomOpMod") || Bukkit.getPluginManager().isPluginEnabled("TotalFreedomMod") || Bukkit.getPluginManager().isPluginEnabled("Frontier"))
        {
            return;
        }
    }
    
    public void strength(Player player, Inventory inv)
    {
        ItemStack i = new ItemStack(Material.IRON_SWORD, 1);
        ItemMeta meta = i.getItemMeta();
        ArrayList<String> lore = new ArrayList<>();
        int level = getSkill(player, "strength");
        meta.setDisplayName(ChatColor.RED + "Strength");
        lore.add(ChatColor.GOLD + "Increases your attack damage");
        lore.add(ChatColor.GOLD + "and reduces damage taken.");
        lore.add(ChatColor.GOLD + "Current: " + level);
        meta.setLore(lore);
        i.setItemMeta(meta);
        inv.setItem(0, i);
    }
    
    public void dexterity(Player player, Inventory inv)
    {
        ItemStack i = new ItemStack(Material.LEATHER_BOOTS, 1);
        ItemMeta meta = i.getItemMeta();
        ArrayList<String> lore = new ArrayList<>();
        int level = getSkill(player, "dexterity");
        meta.setDisplayName(ChatColor.GREEN + "Dexterity");
        lore.add(ChatColor.GOLD + "Increases your speed and evasiveness.");
        lore.add(ChatColor.GOLD + "Current: " + level);
        meta.setLore(lore);
        i.setItemMeta(meta);
        inv.setItem(2, i);
    }
    
    public void intelligence(Player player, Inventory inv)
    {
        ItemStack i = new ItemStack(Material.BOOK, 1);
        ItemMeta meta = i.getItemMeta();
        ArrayList<String> lore = new ArrayList<>();
        int level = getSkill(player, "intelligence");
        meta.setDisplayName(ChatColor.BLUE + "Intelligence");
        lore.add(ChatColor.GOLD + "Increases power of magic based spells");
        lore.add(ChatColor.GOLD + "and reduces mana cost.");
        lore.add(ChatColor.GOLD + "Current: " + level);
        meta.setLore(lore);
        i.setItemMeta(meta);
        inv.setItem(6, i);
    }
    
    public void development(Player player, Inventory inv)
    {
        ItemStack i = new ItemStack(Material.EXP_BOTTLE, 1);
        ItemMeta meta = i.getItemMeta();
        ArrayList<String> lore = new ArrayList<>();
        int level = getSkill(player, "development");
        meta.setDisplayName(ChatColor.DARK_PURPLE + "Development");
        lore.add(ChatColor.GOLD + "Increases the amount of XP gained from spells.");
        lore.add(ChatColor.GOLD + "Current: " + level);
        meta.setLore(lore);
        i.setItemMeta(meta);
        inv.setItem(8, i);
    }
}
