package net.camtech.lgrpg.listeners;

import static net.camtech.lgrpg.LynxGamingRPG.plugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockIgniteEvent;
import static org.bukkit.event.block.BlockIgniteEvent.IgniteCause.FLINT_AND_STEEL;

public class LGRPG_BlockListener implements Listener{
 
    public LGRPG_BlockListener()
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }
    
    @EventHandler
    public void onBlockIgnite(BlockIgniteEvent event)
    {
        if(event.getCause()!= FLINT_AND_STEEL)
        {
            event.setCancelled(true);
        }
    }
}
