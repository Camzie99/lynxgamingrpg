package net.camtech.lgrpg.listeners;

import com.connorlinfoot.bountifulapi.BountifulAPI;
import static java.lang.Math.round;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.CUtils_Particle;
import static net.camtech.lgrpg.LGRPG_BoardManager.updateStats;
import net.camtech.lgrpg.LGRPG_ManaManager;
import net.camtech.lgrpg.LynxGamingRPG;
import static net.camtech.lgrpg.LynxGamingRPG.AbilityManager;
import static net.camtech.lgrpg.LynxGamingRPG.getSkill;
import static net.camtech.lgrpg.LynxGamingRPG.players;
import static net.camtech.lgrpg.LynxGamingRPG.plugin;
import net.camtech.lgrpg.classes.Ability;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.advancement.Advancement;
import static org.bukkit.event.block.Action.LEFT_CLICK_AIR;
import static org.bukkit.event.block.Action.LEFT_CLICK_BLOCK;
import static org.bukkit.event.block.Action.RIGHT_CLICK_AIR;
import static org.bukkit.event.block.Action.RIGHT_CLICK_BLOCK;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class LGRPG_PlayerListener implements Listener
{

    List<Player> moved = new ArrayList<>();
    HashMap<Player, String> currentSpell = new HashMap<>();
    HashMap<Player, Block> blocks = new HashMap<>();

    public LGRPG_PlayerListener()
    {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
    }

    @EventHandler
    public void onEntityDamage(EntityDamageEvent event)
    {
        if (event.getEntity() instanceof Player)
        {
            CUtils_Particle particle = CUtils_Particle.ENDER;
            Player player = (Player) event.getEntity();
            if (player.isSneaking())
            {
                if (event.getCause().equals(DamageCause.FALL))
                {
                    if (LGRPG_ManaManager.takeMana(player, 3 + ((int) player.getFallDistance() / 3)))
                    {
                        event.setCancelled(true);
                        CUtils_Particle.sendToAll(particle, player.getLocation(), 2, 2, 2, 20, 20);
                    }
                    else
                    {
                        LGRPG_ManaManager.setMana(player, 0);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100, 1000));
                        player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 100, -10));
                        event.setCancelled(true);
                        player.setHealth(1d);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onEntityDeath(PlayerDeathEvent event)
    {
        Player player = event.getEntity();
        if (LynxGamingRPG.magic.contains(player.getName()))
        {
            event.setDeathMessage(player.getName() + " was killed by magic!");
        }
    }

    @EventHandler
    public void onPlayerMove(final PlayerMoveEvent event)
    {
        if (moved.contains(event.getPlayer()))
        {
            return;
        }
        event.getPlayer().setFoodLevel(20);
        event.getPlayer().setSaturation(0f);
        moved.add(event.getPlayer());
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                moved.remove(event.getPlayer());
            }
        }.runTaskLater(plugin, 20L * 10L);
    }

    @EventHandler
    public void onPlayerSwitchHotbar(PlayerItemHeldEvent event)
    {
        if (currentSpell.containsKey(event.getPlayer()))
        {
            currentSpell.remove(event.getPlayer());
        }
    }

    @EventHandler
    public void onPlayerDropItem(PlayerDropItemEvent event)
    {
        Player player = event.getPlayer();
        if (currentSpell.containsKey(event.getPlayer()))
        {
            currentSpell.remove(event.getPlayer());
        }
        if (event.getItemDrop().getItemStack().equals(LynxGamingRPG.skillsBook))
        {
            player.sendMessage(ChatColor.BLUE + "You cannot drop your skils book!");
            event.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();
        ItemStack item = player.getItemInHand();
        if (item == null)
        {

        }
        else
        {
            int slot = player.getInventory().first(item);
            boolean weapon = false;
            switch (item.getType())
            {
                case BLAZE_ROD:
                    if (!players.getConfig().getString("players." + player.getName() + ".class").equals("Mage"))
                    {
                        player.sendMessage(ChatColor.RED + "You are not the correct class to use this weapon!");
                        return;
                    }
                    weapon = true;
                    break;
                case IRON_SWORD:
                    if (!players.getConfig().getString("players." + player.getName() + ".class").equals("Warrior"))
                    {
                        player.sendMessage(ChatColor.RED + "You are not the correct class to use this weapon!");
                        return;
                    }
                    weapon = true;
                    break;
                case BOW:
                    event.setCancelled(true);
                    if (!players.getConfig().getString("players." + player.getName() + ".class").equals("Archer"))
                    {
                        player.sendMessage(ChatColor.RED + "You are not the correct class to use this weapon!");
                        return;
                    }
                    weapon = true;
                    break;
            }
            if (weapon)
            {
                if (slot != 0)
                {
                    player.sendMessage(ChatColor.RED + "You must equip this weapon before using it (place it in the first slot fo your hotbar)");
                    return;
                }
                ItemMeta meta = item.getItemMeta();
                if (meta != null)
                {
                    String spell;
                    if (event.getAction() == LEFT_CLICK_AIR || event.getAction() == LEFT_CLICK_BLOCK)
                    {
                        if (currentSpell.containsKey(player))
                        {
                            spell = currentSpell.get(player) + " + Left Click";
                            currentSpell.put(player, spell);
                        }
                        else
                        {
                            currentSpell.put(player, "Left Click");
                        }
                    }
                    if (event.getAction() == RIGHT_CLICK_AIR || event.getAction() == RIGHT_CLICK_BLOCK)
                    {
                        if (currentSpell.containsKey(player))
                        {
                            spell = currentSpell.get(player) + " + Right Click";
                            currentSpell.put(player, spell);
                        }
                        else
                        {
                            currentSpell.put(player, "Right Click");
                        }
                    }
                    BountifulAPI.sendTitle(player, 5, 5, 40, currentSpell.get(player).replaceAll("Right Click", ChatColor.GREEN + "⇨").replaceAll("Left Click", ChatColor.GREEN + "⇦").replaceAll("\\+", ChatColor.GREEN + "+"), "");
                    checkAbilities(player);
                }
            }
            else
            {
                if (item.equals(LynxGamingRPG.skillsBook))
                {
                    openInventory(player);
                }
            }
        }

    }

    @EventHandler
    public void onPlayerSneak(PlayerToggleSneakEvent event)
    {
        final Player player = event.getPlayer();
        if (event.isSneaking())
        {
            if (blocks.containsKey(player))
            {
                return;
            }
            if (player.getLocation().subtract(0, 1, 0).getBlock().getType().isTransparent())
            {
                return;
            }
            if (LGRPG_ManaManager.takeMana(player, 10))
            {
                player.sendBlockChange(player.getLocation().subtract(0, 1, 0), 89, (byte) 0);
                blocks.put(player, player.getLocation().subtract(0, 1, 0).getBlock());
                new BukkitRunnable()
                {
                    @Override
                    public void run()
                    {
                        player.sendBlockChange(blocks.get(player).getLocation(), blocks.get(player).getTypeId(), blocks.get(player).getData());
                        blocks.remove(player);
                    }
                }.runTaskLater(LynxGamingRPG.plugin, 20L * 5L);
            }
        }
    }

    @EventHandler
    public void onPlayerAttack(EntityDamageByEntityEvent event)
    {
        Entity damager = event.getDamager();
        Entity entity = event.getEntity();
        if (damager instanceof Player && entity instanceof Skeleton)
        {
            Player player = (Player) damager;
            String name = player.getName();
            String voc = players.getConfig().getString("players." + name + ".class").toLowerCase();
            int xp = players.getConfig().getInt("players." + name + ".classes." + voc + ".xp");
            players.getConfig().set("players." + name + ".classes." + voc + ".xp", xp + (event.getDamage() / 2));
            players.saveConfig();
            updateStats(player);
        }
    }

    public void checkAbilities(Player player)
    {
        String vocation = players.getConfig().getString("players." + player.getName() + ".class");
        int level = players.getConfig().getInt("players." + player.getName() + ".classes." + vocation.toLowerCase() + ".level");

        for (Ability abil : AbilityManager.Abilities)
        {
            if (abil.getVoc().equals(vocation))
            {
                if (abil.getCode().equals(currentSpell.get(player)) && (abil.isEvolved() == player.isSneaking()))
                {
                    if (level < abil.getLevel())
                    {
                        player.sendMessage(ChatColor.RED + "You are not a high enough level to use " + ChatColor.GOLD + abil.getName());
                    }
                    else if(LGRPG_ManaManager.takeMana(player, abil.getMana()))
                    {
                        abil.run(player);
                        double multiplier = getSkill(player, "development") / 2;
                        if (multiplier < 1)
                        {
                            multiplier = 1.0;
                        }
                        LynxGamingRPG.AbilityManager.addXp(player, round(abil.getXp() * multiplier));
                        BountifulAPI.sendTitle(player, 5, 5, 40, ChatColor.AQUA + "You cast " + ChatColor.GOLD + abil.getName(), ChatColor.AQUA + "(- " + ChatColor.GOLD + abil.getMana() + ChatColor.AQUA + " mana).");
                    }
                }
            }
        }
        int count = 0;
        for (String word : currentSpell.get(player).split(" "))
        {
            if (word.contains("Click"))
            {
                count++;
            }
        }
        if (count >= 3)
        {
            currentSpell.remove(player);
        }
    }

    public static void openInventory(Player player)
    {
        int points = players.getConfig().getInt("players." + player.getName() + ".points");
        Inventory inv = Bukkit.createInventory(null, 9, ChatColor.BLUE + "Skills");
        for (String skill : LynxGamingRPG.skills)
        {
            ItemStack i;
            ItemMeta meta;
            int level = players.getConfig().getInt("players." + player.getName() + ".skills." + skill);
            ArrayList<String> lore = new ArrayList<>();
            if (skill.equals("strength"))
            {
                i = new ItemStack(Material.IRON_SWORD, 1);
                meta = i.getItemMeta();
                meta.setDisplayName(ChatColor.RED + "Strength");
                lore.add(ChatColor.GOLD + "Increases your attack damage");
                lore.add(ChatColor.GOLD + "and reduces damage taken.");
                lore.add(ChatColor.GOLD + "Current: " + level);
                meta.setLore(lore);
                i.setItemMeta(meta);
                inv.setItem(0, i);
            }
            if (skill.equals("dexterity"))
            {
                i = new ItemStack(Material.LEATHER_BOOTS, 1);
                meta = i.getItemMeta();
                meta.setDisplayName(ChatColor.GREEN + "Dexterity");
                lore.add(ChatColor.GOLD + "Increases your speed and evasivness.");
                lore.add(ChatColor.GOLD + "Current: " + level);
                meta.setLore(lore);
                i.setItemMeta(meta);
                inv.setItem(2, i);
            }
            if (skill.equals("intelligence"))
            {
                i = new ItemStack(Material.BOOK, 1);
                meta = i.getItemMeta();
                meta.setDisplayName(ChatColor.BLUE + "Intelligence");
                lore.add(ChatColor.GOLD + "Increases power of magic based spells");
                lore.add(ChatColor.GOLD + "and increases maxium mana.");
                lore.add(ChatColor.GOLD + "Current: " + level);
                meta.setLore(lore);
                i.setItemMeta(meta);
                inv.setItem(6, i);
            }
            if (skill.equals("development"))
            {
                i = new ItemStack(Material.EXP_BOTTLE, 1);
                meta = i.getItemMeta();
                meta.setDisplayName(ChatColor.DARK_PURPLE + "Development");
                lore.add(ChatColor.GOLD + "Increases the amount of XP gained from spells.");
                lore.add(ChatColor.GOLD + "Current: " + level);
                meta.setLore(lore);
                i.setItemMeta(meta);
                inv.setItem(8, i);
            }
            ItemStack item = new ItemStack(Material.ENCHANTED_BOOK, 1);
            ItemMeta itemmeta = item.getItemMeta();
            ArrayList<String> lore2 = new ArrayList<>();
            lore2.add(ChatColor.GOLD + "Current Skill Points: " + points);
            itemmeta.setDisplayName(ChatColor.LIGHT_PURPLE + "Skill Points");
            itemmeta.setLore(lore2);
            item.setItemMeta(itemmeta);
            inv.setItem(4, item);
        }
        player.openInventory(inv);
    }
}
