package net.camtech.lgrpg.classes;

import java.util.HashMap;
import me.joseph.particle.api.ParticleAPI;
import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.CUtils_Player;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import net.camtech.lgrpg.LynxGamingRPG;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Abil_Grab extends Ability
{

    public HashMap<String, Integer> timeLeft = new HashMap<>();

    public Abil_Grab()
    {
        super("Grab", "Left Click + Right Click + Right Click", "Mage", 17, 5, 0, true);
    }

    @Override
    public void run(Player player)
    {
        CUtils_Player cplayer = new CUtils_Player(player);
        if (cplayer.isTargetingEntity())
        {
            LivingEntity e = cplayer.getTargetEntity();
            timeLeft.put(player.getName(), 5);
            takeTime(player);
            grab(player, e);
            LynxGamingRPG.AbilityManager.addXp(player, 30);
        }
        else
        {
            player.sendMessage(ChatColor.RED + "You are not targeting an entity!");
        }
    }

    public void grab(final Player player, final LivingEntity entity)
    {
        final CUtils_Player cplayer = new CUtils_Player(player);

        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                if (!timeLeft.containsKey(player.getName()))
                {
                    return;
                }
                if (entity instanceof Player)
                {
                    ((Player) entity).setAllowFlight(true);
                }
                Location loc = cplayer.getTargetBlock(20);
                entity.teleport(new Location(entity.getWorld(), loc.getX(), loc.getY() + 0.5, loc.getZ()));
                int radius = 1;
                for (double y = 0; y <= 27.6; y += 0.2)
                {
                    double x = radius * Math.cos(y);
                    double z = radius * Math.sin(y);
                    PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(net.minecraft.server.v1_12_R1.EnumParticle.ENCHANTMENT_TABLE, true, (float) (loc.getX() + x), (float) (loc.getY() + (y - 12.5) / 7.5), (float) (loc.getZ() + z), 0, 0, 0, 0, 1);
                    PacketPlayOutWorldParticles packetTwo = new PacketPlayOutWorldParticles(net.minecraft.server.v1_12_R1.EnumParticle.ENCHANTMENT_TABLE, true, (float) (loc.getX() + z), (float) (loc.getY() + (y - 12.5) / 7.5), (float) (loc.getZ() + x), 0, 0, 0, 0, 1);
                    for (Player online : Bukkit.getOnlinePlayers())
                    {
                        ((CraftPlayer) online).getHandle().playerConnection.sendPacket(packet);
                        ((CraftPlayer) online).getHandle().playerConnection.sendPacket(packetTwo);
                    }
                }
                entity.setVelocity(new Vector(0, 0, 0));
                if (entity instanceof Player)
                {
                    Player p = (Player) entity;
                    if (p.getGameMode() != GameMode.CREATIVE)
                    {
                        p.setAllowFlight(false);
                    }
                }
                grab(player, entity);
            }
        }.runTaskLater(LynxGamingRPG.plugin, 5L);
    }

    public void takeTime(final Player player)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                if (timeLeft.containsKey(player.getName()))
                {
                    int time = timeLeft.get(player.getName());
                    if (time == 0)
                    {
                        timeLeft.remove(player.getName());
                        return;
                    }
                    else
                    {
                        timeLeft.remove(player.getName());
                        timeLeft.put(player.getName(), time - 1);
                    }
                }
                else
                {
                    timeLeft.put(player.getName(), 5);
                }
                takeTime(player);
            }
        }.runTaskLater(LynxGamingRPG.plugin, 20L);
    }
    
    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_grab", "Grab", "Use the Grab spell.", "minecraft:iron_bars");
    }
}
