package net.camtech.lgrpg.classes;

import net.camtech.camutils.CUtils_Advancement;
import org.bukkit.entity.Player;

public abstract class Ability
{

    private final String name;

    private final String code;

    private final String vocation;

    private final int level;

    private final int mana;

    private final int xp;

    private final boolean evolved;

    public Ability(String name, String code, String vocation, int level, int mana, int xp)
    {
        this.name = name;
        this.code = code;
        this.vocation = vocation;
        this.level = level;
        this.mana = mana;
        this.xp = xp;
        this.evolved = false;
    }

    public Ability(String name, String code, String vocation, int level, int mana, int xp, boolean evolved)
    {
        this.name = name;
        this.code = code;
        this.vocation = vocation;
        this.level = level;
        this.mana = mana;
        this.xp = xp;
        this.evolved = evolved;
    }

    public String getName()
    {
        return this.name;
    }

    public String getCode()
    {
        return this.code;
    }

    public String getVoc()
    {
        return this.vocation;
    }

    public int getLevel()
    {
        return this.level;
    }

    public int getMana()
    {
        return this.mana;
    }

    public int getXp()
    {
        return this.xp;
    }
    
    public boolean isEvolved()
    {
        return this.evolved;
    }

    public abstract void run(Player player);
    
    public abstract CUtils_Advancement getAdvancement();

}
