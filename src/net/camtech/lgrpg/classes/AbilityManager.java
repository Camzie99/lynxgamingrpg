package net.camtech.lgrpg.classes;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.CodeSource;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import net.camtech.lgrpg.LGRPG_BoardManager;
import net.camtech.lgrpg.LynxGamingRPG;
import static net.camtech.lgrpg.LynxGamingRPG.mainconfig;
import static net.camtech.lgrpg.LynxGamingRPG.players;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Sound;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;

public class AbilityManager
{

    public ArrayList<Ability> Abilities = new ArrayList<>();

    public AbilityManager()
    {
        for (Ability abil : AbilityClasses())
        {
            this.Abilities.add(abil);
        }
    }

    Class noparams[] =
    {
    };

    public ArrayList<Ability> AbilityClasses()
    {
        ArrayList<Ability> abils = new ArrayList<>();
        try
        {
            Pattern PATTERN = Pattern.compile("net/camtech/lgrpg/classes/(Abil_[^\\$]+)\\.class");
            CodeSource codeSource = LynxGamingRPG.class.getProtectionDomain().getCodeSource();
            if (codeSource != null)
            {
                ZipInputStream zip = new ZipInputStream(codeSource.getLocation().openStream());
                ZipEntry zipEntry;
                while ((zipEntry = zip.getNextEntry()) != null)
                {
                    String entryName = zipEntry.getName();
                    Matcher matcher = PATTERN.matcher(entryName);
                    if (matcher.find())
                    {
                        try
                        {
                            Class<?> abilClass = Class.forName("net.camtech.lgrpg.classes." + matcher.group(1));
                            Constructor construct = abilClass.getConstructor();
                            Ability abil = (Ability) construct.newInstance();
                            abils.add(abil);
                        }
                        catch (ClassNotFoundException | NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex)
                        {
                            Bukkit.broadcastMessage("" + ex);
                        }
                    }
                }
            }
        }
        catch (IOException ex)
        {
            Bukkit.broadcastMessage("" + ex);
        }
        return abils;
    }

    public void addXp(Player player, double Xp)
    {
        String vocation = players.getConfig().getString("players." + player.getName() + ".class");
        String xp = "players." + player.getName() + ".classes." + vocation.toLowerCase() + ".xp";
        String toNext = xp.replaceAll(".xp", ".toNext");
        String level = xp.replaceAll(".xp", ".level");
        if (players.getConfig().getInt(level) == mainconfig.getConfig().getInt("Classes.maxLevel"))
        {
            players.getConfig().set(toNext, 1);
        }
        players.getConfig().set(xp, players.getConfig().getDouble(xp) + Xp);
        if (players.getConfig().getDouble(xp) >= players.getConfig().getDouble(toNext) && players.getConfig().getDouble(toNext) != 1)
        {
            int points = players.getConfig().getInt("players." + player.getName() + ".points");
            player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 1, 1);
            launchFirework(player);
            players.getConfig().set("players." + player.getName() + ".points", points + 5);
            player.sendMessage(ChatColor.AQUA + "Level Up!");
            players.getConfig().set(level, players.getConfig().getInt(level) + 1);
            players.getConfig().set(xp, 0);
            players.saveConfig();
            for (Ability abil : LynxGamingRPG.AbilityManager.Abilities)
            {
                if (abil.getLevel() == players.getConfig().getInt(level) && abil.getVoc().equals(vocation))
                {
                    player.sendMessage(ChatColor.AQUA + "You now have access to " + ChatColor.GOLD + abil.getName() + ChatColor.AQUA + " (" + ChatColor.GOLD + abil.getCode() + ChatColor.AQUA + ")!");
                }
            }
        }
        LGRPG_BoardManager.updateStats(player);
    }

    public void launchFirework(Player player)
    {
        Firework fw = (Firework) player.getWorld().spawnEntity(player.getLocation(), EntityType.FIREWORK);
        FireworkMeta fwm = fw.getFireworkMeta();

        Random r = new Random();
        int rt = r.nextInt(4) + 1;
        Type type = Type.BALL;
        if (rt == 1)
        {
            type = Type.BALL;
        }
        if (rt == 2)
        {
            type = Type.BALL_LARGE;
        }
        if (rt == 3)
        {
            type = Type.BURST;
        }
        if (rt == 4)
        {
            type = Type.CREEPER;
        }
        if (rt == 5)
        {
            type = Type.STAR;
        }

        Color c1 = Color.AQUA;
        Color c2 = Color.GREEN;
        FireworkEffect effect = FireworkEffect.builder().flicker(r.nextBoolean()).withColor(c1).withFade(c2).with(type).trail(r.nextBoolean()).build();
        fwm.addEffect(effect);
        int rp = r.nextInt(2) + 1;
        fwm.setPower(rp);
        fw.setFireworkMeta(fwm);
    }
}
