package net.camtech.lgrpg.classes;

import java.util.ArrayList;
import net.camtech.camutils.CUtils_Advancement;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import net.camtech.lgrpg.LynxGamingRPG;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Abil_Rally extends Ability{

    public Abil_Rally()
    {
        super("Rally", "Right Click + Left Click + Right Click", "Warrior", 3, 3, 0);
    }

    @Override
    public void run(Player player)
    {
        ArrayList<Player> players = new ArrayList<>();
        for(Player p : Bukkit.getOnlinePlayers())
        {
            if(p.getLocation().distance(player.getLocation()) <= 5)
            {
                players.add(p);
            }
        }
        int i=0;
        
        for(Player p : players)
        {
            p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 20 * 10, 1));
            p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 20 * 10, 1));
            i++;
        }
        
        LynxGamingRPG.AbilityManager.addXp(player, i * 15);
    }
    
    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_rally", "Rally", "Use the Rally ability.", "minecraft:banner");
    }
}
