package net.camtech.lgrpg.classes;

import me.joseph.particle.api.ParticleAPI;
import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.CUtils_Particle;
import net.camtech.camutils.CUtils_Player;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import net.camtech.lgrpg.LynxGamingRPG;
import static net.camtech.lgrpg.LynxGamingRPG.magic;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

public class Abil_Smash extends Ability
{

    public Abil_Smash()
    {
        super("Smash", "Right Click + Right Click + Right Click", "Warrior", 1, 3, 0);
    }

    @Override
    public void run(Player player)
    {
        CUtils_Player cplayer = new CUtils_Player(player);
        if (!cplayer.isTargetingEntity(7))
        {
            player.sendMessage(ChatColor.RED + "You are not targetting an entity");
        }
        else
        {
            final LivingEntity e = cplayer.getTargetEntity(7);
            if (e instanceof Player && !magic.contains(((Player) e).getName()))
            {
                magic.add(((Player) e).getName());
                new BukkitRunnable()
                {
                    @Override
                    public void run()
                    {
                        magic.remove(((Player) e).getName());
                    }
                }.runTaskLater(LynxGamingRPG.plugin, 20L * 1L);
            }

            ParticleAPI effect = (ParticleAPI) Bukkit.getPluginManager().getPlugin("ParticleAPI");
            effect.playEffect(player, "EXPLOSION_HUGE", e.getLocation(), 0, 0, 0, 3, 3);
            double damage = 10d;
            if (player.hasPotionEffect(PotionEffectType.INCREASE_DAMAGE))
            {
                damage = damage * 1.8;
            }
            e.damage(damage);
            LynxGamingRPG.AbilityManager.addXp(player, 10);
        }
    }

    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_smash", "Smash", "Use the Smash ability.", "minecraft:iron_sword");
    }
}
