package net.camtech.lgrpg.classes;

import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.CUtils_Player;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Abil_Teleport extends Ability
{

    public Abil_Teleport()
    {
        super("Teleport", "Left Click + Left Click + Left Click", "Mage", 7, 4, 20);
    }

    @Override
    public void run(Player player)
    {
        CUtils_Player cplayer = new CUtils_Player(player);
        Location loc = cplayer.getTargetBlock(20).add(0d, 2d, 0d);
        loc.setDirection(player.getLocation().getDirection());
        player.teleport(loc);
    }

    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_teleport", "Teleport", "Use the Teleport spell.", "minecraft:ender_pearl");
    }
}
