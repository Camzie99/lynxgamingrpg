package net.camtech.lgrpg.classes;

import java.util.ArrayList;
import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.Tracing;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import net.camtech.lgrpg.LynxGamingRPG;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemStack;

public class Abil_Freeze extends Ability
{

    public Abil_Freeze()
    {
        super("Freeze", "Right Click + Left Click + Right Click", "Mage", 5, 4, 5);
    }

    @Override
    public void run(final Player player)
    {
        final Location loc = Tracing.getTarget(player, 30).loc.add(0, 2, 0);
        trigger(player, loc, 0);
    }

    private void trigger(final Player player, final Location loc, final int times)
    {
        if (times >= 20)
        {
            return;
        }
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                float radius = 2.5f;
                for (LivingEntity e : loc.getWorld().getLivingEntities())
                {
                    if (e.getLocation().distance(loc) <= radius && !e.equals(player))
                    {
                        e.damage(0.04 * LynxGamingRPG.getSkill(player, "intelligence"));
                        e.teleport(loc);
                        LynxGamingRPG.AbilityManager.addXp(player, 5);
                    }
                }
                ArrayList<PacketPlayOutWorldParticles> packets = new ArrayList<>();
                for (float x = -radius; x <= radius; x += radius / 2)
                {
                    for (float y = -radius; y <= radius; y += radius / 2)
                    {
                        for (float z = -radius; z <= radius; z += radius / 2)
                        {
                            if (loc.distance(loc.clone().add(x, y, z)) <= radius)
                            {
                                packets.add(new PacketPlayOutWorldParticles(EnumParticle.SNOW_SHOVEL, true, (float) (loc.getX() + x), (float) (loc.getY() + y), (float) (loc.getZ() + z), 0, 0, 0, 0.125f, 5));
                            }
                        }
                    }
                }
                for (PacketPlayOutWorldParticles packet : packets)
                {
                    for (Player online : Bukkit.getOnlinePlayers())
                    {
                        ((CraftPlayer) online).getHandle().playerConnection.sendPacket(packet);
                    }
                }
                trigger(player, loc, times + 1);
            }
        }.runTaskLater(LynxGamingRPG.plugin, 4L);
    }

    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_freeze", "Freeze", "Use the Freeze spell.", "minecraft:ice");
    }
}
