package net.camtech.lgrpg.classes;

import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.CUtils_Player;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Abil_Launch extends Ability{
    
    public Abil_Launch()
    {
        super("Launch", "Right Click + Left Click + Left Click", "Mage", 7, 5, 25);
    }
    
    @Override
    public void run(final Player player)
    {
        CUtils_Player cplayer = new CUtils_Player(player);
        if (!cplayer.isTargetingEntity()) 
        {
            player.setVelocity(player.getLocation().getDirection().multiply(5));
        } 
        else 
        {
            cplayer.getTargetEntity().setVelocity(cplayer.getTargetEntity().getLocation().getDirection().multiply(-5));
        }
    }
    
    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_launch", "Launch", "Use the Launch spell.", "minecraft:piston");
    }
}
