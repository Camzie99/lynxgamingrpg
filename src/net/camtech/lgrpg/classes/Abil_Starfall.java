package net.camtech.lgrpg.classes;

import java.util.Random;
import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.CUtils_Player;
import net.camtech.camutils.Tracing;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import static net.camtech.lgrpg.LynxGamingRPG.getSkill;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class Abil_Starfall extends Ability
{

    public Abil_Starfall()
    {
        super("Starfall", "Left Click + Left Click + Right Click", "Mage", 10, 5, 15);
    }

    @Override
    public void run(Player player)
    {
        CUtils_Player cplayer = new CUtils_Player(player);
        Random rand = new Random();
        Vector random = new Vector(rand.nextInt(20), 0, rand.nextInt(20));
        if (rand.nextBoolean())
        {
            random.setX(-random.getX());
        }
        if (rand.nextBoolean())
        {
            random.setZ(-random.getZ());
        }
        Location to = Tracing.getTarget(player, 30).loc;
        for(LivingEntity entity : player.getWorld().getLivingEntities())
        {
            if(to.distance(entity.getLocation()) <= 4)
            {
                entity.damage(0.1 * getSkill(player, "intelligence"));
            }
        }
        Location origin = to.clone().add(random).add(0,60,0);
        origin.setDirection(to.clone().toVector().subtract(origin.toVector()));
        Vector increase = origin.getDirection();
        for (int counter = 0; counter <= 60; counter++)
        {
            Location loc = origin.add(increase); // add the increase
            PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(net.minecraft.server.v1_12_R1.EnumParticle.END_ROD, true, (float) (loc.getX()), (float) (loc.getY()), (float) (loc.getZ()), 0, 0, 0, 0.1f, 25);
            for (Player online : Bukkit.getOnlinePlayers())
            {
                ((CraftPlayer) online).getHandle().playerConnection.sendPacket(packet);
            }
        }
    }
    
    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_starfall", "Starfall", "Use the Starfall spell.", "minecraft:nether_star");
    }
}
