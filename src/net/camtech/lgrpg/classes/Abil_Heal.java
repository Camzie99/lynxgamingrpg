package net.camtech.lgrpg.classes;

import me.joseph.particle.api.ParticleAPI;
import net.camtech.camutils.CUtils_Advancement;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import static net.camtech.lgrpg.LynxGamingRPG.players;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Abil_Heal extends Ability
{

    public Abil_Heal()
    {
        super("Heal", "Right Click + Right Click + Left Click", "Mage", 3, 4, 15);
    }

    @Override
    public void run(Player player)
    {
        String vocation = players.getConfig().getString("players." + player.getName() + ".class").toLowerCase();
        int level = players.getConfig().getInt("players." + player.getName() + ".classes." + vocation + ".level");
        int add = 6 + (level / 3);
        if (player.getHealth() >= (player.getMaxHealth() - add))
        {
            player.setHealth(player.getHealth() + (player.getMaxHealth() - player.getHealth()));
        }
        else
        {
            player.setHealth(player.getHealth() + 6);
        }
        ParticleAPI effect = (ParticleAPI) Bukkit.getPluginManager().getPlugin("ParticleAPI");
        effect.playEffect(player, "HEART", player.getLocation(), 0, 0, 0, 1, 100);
        player.sendMessage(ChatColor.AQUA + "+" + ChatColor.RED + (add / 2) + ChatColor.AQUA + " hearts.");
        for (Entity e : player.getNearbyEntities(2, 2, 2))
        {
            if (e instanceof Player)
            {
                Player p = (Player) e;
                if (p.getHealth() >= p.getMaxHealth() - add)
                {
                    p.setHealth(p.getHealth() + (p.getMaxHealth() - p.getHealth()));
                }
                else
                {
                    p.setHealth(p.getHealth() + add);
                }
                effect.playEffect(p, "HEART", p.getLocation(), 0, 0, 0, 1, 100);
                p.sendMessage(ChatColor.AQUA + "+" + ChatColor.RED + (add / 2) + ChatColor.AQUA + " hearts.");
            }
        }
    }

    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_heal", "Heal", "Use the Heal spell.", "minecraft:apple");
    }
}
