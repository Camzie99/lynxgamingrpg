package net.camtech.lgrpg.classes;

import net.camtech.camutils.CUtils_Advancement;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import net.camtech.lgrpg.LGRPG_MovingEffect;
import net.camtech.lgrpg.LynxGamingRPG;
import static net.camtech.lgrpg.LynxGamingRPG.getSkill;
import org.bukkit.entity.Player;
import net.minecraft.server.v1_12_R1.EnumParticle;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

public class Abil_Fireball extends Ability
{

    public Abil_Fireball()
    {
        super("Fireball", "Right Click + Right Click + Right Click", "Mage", 1, 4, 10);
    }

    @Override
    public void run(final Player player)
    {
        new LGRPG_MovingEffect(EnumParticle.FLAME, player.getLocation().getDirection().multiply(100), player.getLocation(), 2, 2.5f + (0.05f * LynxGamingRPG.getSkill(player, "intelligence")), 100)
        {
            @Override
            public void onHitEntity(LivingEntity entity)
            {
                if (!entity.equals(player))
                {
                    entity.damage(0.1 * getSkill(player, "intelligence"));
                    LynxGamingRPG.AbilityManager.addXp(player, 5);
                }
            }
        }.move();
    }
    
    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_fireball", "Fireball", "Use the Fireball spell.", "minecraft:fire_charge");
    }

}
