package net.camtech.lgrpg.classes;

import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.CUtils_Player;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import static net.camtech.lgrpg.LynxGamingRPG.players;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class Abil_Explode extends Ability{

    public Abil_Explode()
    {
        super("Explode", "Left Click + Right Click + Left Click", "Mage", 15, 8, 50);
    }
    
    @Override
    public void run(Player player)
    {
        CUtils_Player cplayer = new CUtils_Player(player);
        String vocation = players.getConfig().getString("players." + player.getName() + ".class");
        int level = players.getConfig().getInt("players." + player.getName() + ".classes." + vocation + ".level");
        World world = player.getWorld();
        Location loc = cplayer.getTargetBlock(20);
        world.createExplosion(loc.getX(), loc.getY() + 1, loc.getZ(), 3 + (level / 2), false, false);
    }
    
    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_explode", "Explode", "Use the Explode spell.", "minecraft:tnt");
    }
}
