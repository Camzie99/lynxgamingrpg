package net.camtech.lgrpg.classes;

import java.util.HashMap;
import java.util.Random;
import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.CUtils_Player;
import net.camtech.camutils.Tracing;
import net.camtech.lgrpg.LGRPG_AdvancementManager;
import net.camtech.lgrpg.LynxGamingRPG;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class Abil_Tornado extends Ability
{

    public HashMap<String, Integer> timeLeft = new HashMap<>();
    public HashMap<String, Float> runs = new HashMap<>();

    public Abil_Tornado()
    {
        super("Tornado", "Left Click + Right Click + Right Click", "Mage", 7, 5, 0, false);
    }

    @Override
    public void run(Player player)
    {
        CUtils_Player cplayer = new CUtils_Player(player);
        timeLeft.put(player.getName(), 5);
        takeTime(player);
        Location loc = Tracing.getTarget(player, 30).loc.add(0, 2, 0);
        tornado(player, loc);
        LynxGamingRPG.AbilityManager.addXp(player, 30);
    }

    public void tornado(final Player player, final Location loc)
    {
        final CUtils_Player cplayer = new CUtils_Player(player);

        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                if (!timeLeft.containsKey(player.getName()))
                {
                    return;
                }
                if (!runs.containsKey(player.getName()))
                {
                    runs.put(player.getName(), 0f);
                }
                else
                {
                    runs.put(player.getName(), runs.get(player.getName()) + 0.5f);
                }
                for (LivingEntity entity : player.getWorld().getLivingEntities())
                {
                    if (entity.getLocation().distance(loc) <= 3)
                    {
                        entity.setVelocity(new Vector(0, 1, 0));
                        LynxGamingRPG.AbilityManager.addXp(player, 5);
                    }
                }
                int radius = 8;
                for (double y = 0; y <= 27.7; y += 0.2)
                {
                    double x = radius * Math.cos(y + runs.get(player.getName()));
                    double z = radius * Math.sin(y + runs.get(player.getName()));
                    PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(net.minecraft.server.v1_12_R1.EnumParticle.CLOUD, true, (float) (loc.getX() + x / (27.8 - y / 1.15)), (float) (loc.getY() + (y - 12.5) / 5), (float) (loc.getZ() + z / (27.8 - y / 1.15)), 0, 0, 0, 0, 1);
                    PacketPlayOutWorldParticles packetTwo = new PacketPlayOutWorldParticles(net.minecraft.server.v1_12_R1.EnumParticle.CLOUD, true, (float) (loc.getX() + z / (27.8 - y / 1.15)), (float) (loc.getY() + (y - 12.5) / 5), (float) (loc.getZ() + x / (27.8 - y / 1.15)), 0, 0, 0, 0, 1);
                    for (Player online : Bukkit.getOnlinePlayers())
                    {
                        ((CraftPlayer) online).getHandle().playerConnection.sendPacket(packet);
                        ((CraftPlayer) online).getHandle().playerConnection.sendPacket(packetTwo);
                    }
                }
                tornado(player, loc);
            }
        }.runTaskLater(LynxGamingRPG.plugin, 2L);
    }

    public void takeTime(final Player player)
    {
        new BukkitRunnable()
        {
            @Override
            public void run()
            {
                if (timeLeft.containsKey(player.getName()))
                {
                    int time = timeLeft.get(player.getName());
                    if (time == 0)
                    {
                        timeLeft.remove(player.getName());
                        return;
                    }
                    else
                    {
                        timeLeft.remove(player.getName());
                        timeLeft.put(player.getName(), time - 1);
                    }
                }
                else
                {
                    timeLeft.put(player.getName(), 5);
                }
                takeTime(player);
            }
        }.runTaskLater(LynxGamingRPG.plugin, 20L);
    }
    
    @Override
    public CUtils_Advancement getAdvancement()
    {
        return LGRPG_AdvancementManager.createAdvancement("abil_tornado", "Tornado", "Use the Tornado spell.", "minecraft:wool");
    }

}
