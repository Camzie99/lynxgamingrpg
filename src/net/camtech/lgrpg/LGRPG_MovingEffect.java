package net.camtech.lgrpg;

import java.util.ArrayList;
import org.bukkit.entity.LivingEntity;
import org.bukkit.util.Vector;
import net.minecraft.server.v1_12_R1.EnumParticle;
import net.minecraft.server.v1_12_R1.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class LGRPG_MovingEffect
{

    private Vector direction;
    private Vector position;
    private long delay;
    private int maxTimes, times;
    private float radius;
    private EnumParticle particle;
    private World world;

    public LGRPG_MovingEffect(EnumParticle particle, Vector direction, Location position, long delay, float radius, int maxTimes)
    {
        this.particle = particle;
        this.direction = direction.normalize();
        this.position = position.toVector();
        this.delay = delay;
        this.radius = radius;
        this.maxTimes = maxTimes;
        this.times = 0;
        this.world = position.getWorld();
    }

    public void move()
    {
        if (times++ <= maxTimes)
        {
            new BukkitRunnable()
            {
                @Override
                public void run()
                {
                    position.add(direction);
                    for (LivingEntity entity : world.getLivingEntities())
                    {
                        if (entity.getLocation().toVector().distance(position) <= radius)
                        {
                            onHitEntity(entity);
                        }
                    }
                    ArrayList<PacketPlayOutWorldParticles> packets = new ArrayList<>();
                    Location loc = position.toLocation(Bukkit.getWorld("world"));
                    for (float x = -radius; x <= radius; x += radius / 4)
                    {
                        for (float y = -radius; y <= radius; y += radius / 4)
                        {
                            for (float z = -radius; z <= radius; z += radius / 4)
                            {
                                if (loc.distance(loc.clone().add(x, y, z)) <= radius)
                                {
                                    packets.add(new PacketPlayOutWorldParticles(particle, true, (float) (loc.getX() + x), (float) (loc.getY() + y), (float) (loc.getZ() + z), 0, 0, 0, 0.125f, 5 + (int) Math.ceil(radius / 5)));
                                }
                            }
                        }
                    }
                    for (PacketPlayOutWorldParticles packet : packets)
                    {
                        for (Player player : Bukkit.getOnlinePlayers())
                        {
                            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
                        }
                    }
                    move();
                }
            }.runTaskLater(LynxGamingRPG.plugin, delay);
        }
    }

    public abstract void onHitEntity(LivingEntity entity);
}
