package net.camtech.lgrpg;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.camtech.camutils.CUtils_Advancement;
import net.camtech.camutils.CUtils_Config;
import static net.camtech.lgrpg.LGRPG_BoardManager.updateStats;
import net.camtech.lgrpg.classes.AbilityManager;
import net.camtech.lgrpg.commands.Command_class;
import net.camtech.lgrpg.commands.Command_getbook;
import net.camtech.lgrpg.commands.Command_level;
import net.camtech.lgrpg.commands.Command_points;
import net.camtech.lgrpg.commands.Command_retab;
import net.camtech.lgrpg.commands.Command_tutorial;
import net.camtech.lgrpg.listeners.LGRPG_BlockListener;
import net.camtech.lgrpg.listeners.LGRPG_ClassListener;
import net.camtech.lgrpg.listeners.LGRPG_ConnectionListener;
import net.camtech.lgrpg.listeners.LGRPG_PlayerListener;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.NamespacedKey;

public class LynxGamingRPG extends JavaPlugin
{

    public final Logger logger = Logger.getLogger("Minecraft");

    public static CUtils_Config players;
    public static CUtils_Config mainconfig;
    public static LynxGamingRPG plugin;
    public static AbilityManager AbilityManager;
    public static ArrayList<String> magic = new ArrayList<>();
    public static ArrayList<String> skills = new ArrayList<>();
    public static ItemStack skillsBook;

    @Override
    public void onEnable()
    {
        PluginDescriptionFile pdfFile = this.getDescription();
        this.logger.log(Level.INFO, "{0}{1} has been enabled!", new Object[]
        {
            pdfFile.getName(), pdfFile.getVersion()
        });
        if (pdfFile.getVersion().toLowerCase().contains("alpha"))
        {
            this.logger.log(Level.WARNING, "{0} IS IN A VERY UN-TESTED ALPHA, DO NOT BE SURPRISED IF SOMETHING GOES WRONG!", pdfFile.getName());
        }
        if (pdfFile.getVersion().toLowerCase().contains("beta"))
        {
            this.logger.warning("This is a beta build, it should be fairly stable but there may be a few errors, please report them on the bug tracker!");
        }
        this.plugin = this;
        this.players = new CUtils_Config(plugin, "players.yml");
        this.mainconfig = new CUtils_Config(plugin, "config.yml");
        this.AbilityManager = new AbilityManager();
        ItemStack book = new ItemStack(Material.ENCHANTED_BOOK, 1);
        ItemMeta meta = book.getItemMeta();
        meta.setDisplayName(ChatColor.BLUE + "Skills");
        ArrayList<String> lore = new ArrayList<>();
        lore.add(ChatColor.GOLD + "A mysterious book which");
        lore.add(ChatColor.GOLD + "accounts your developments");
        lore.add(ChatColor.GOLD + "throughout this journey..." + ChatColor.RESET);
        meta.setLore(lore);
        book.setItemMeta(meta);
        this.skillsBook = book;
        this.skills.add("strength");
        this.skills.add("dexterity");
        this.skills.add("intelligence");
        this.skills.add("development");
        this.getCommand("class").setExecutor(new Command_class());
        this.getCommand("level").setExecutor(new Command_level());
        this.getCommand("retab").setExecutor(new Command_retab());
        this.getCommand("points").setExecutor(new Command_points());
        this.getCommand("tutorial").setExecutor(new Command_tutorial());
        this.getCommand("getbook").setExecutor(new Command_getbook());
        LGRPG_PlayerListener playerListener = new LGRPG_PlayerListener();
        LGRPG_ConnectionListener connectionListener = new LGRPG_ConnectionListener();
        LGRPG_BlockListener blockListener = new LGRPG_BlockListener();
        LGRPG_ClassListener classListener = new LGRPG_ClassListener();
        //Save Configs
        players.saveDefaultConfig();
        mainconfig.saveDefaultConfig();
        LGRPG_ManaManager.refillMana();
        for (Player player : Bukkit.getOnlinePlayers())
        {
            updateStats(player);
        }
    }

    @Override
    public void onDisable()
    {
        PluginDescriptionFile pdfFile = this.getDescription();
        this.logger.log(Level.INFO, "{0} has been disabled! ~ Goodbye!", pdfFile.getName());
        for (Player player : Bukkit.getOnlinePlayers())
        {
            updateStats(player);
        }
    }

    public static int getSkill(Player player, String skill)
    {
        return players.getConfig().getInt("players." + player.getName() + ".skills." + skill);
    }
}
